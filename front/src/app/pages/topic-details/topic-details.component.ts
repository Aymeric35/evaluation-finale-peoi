import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { Answer } from 'src/app/models/answer';
import { Topic } from 'src/app/models/topic';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { TopicService } from 'src/app/services/topic.service';

@Component({
  selector: 'app-topic-details',
  templateUrl: './topic-details.component.html',
  styleUrls: ['./topic-details.component.scss']
})
export class TopicDetailsComponent implements OnInit {
  public topic?: Topic
  public answerContent: string = "";

  constructor(public authenticationService: AuthenticationService, private topicService: TopicService, private activeRoute: ActivatedRoute, private router: Router) { }

  async getTopic() {
    this.activeRoute.params.subscribe(async params => {
      this.topic = await this.topicService.getTopicById(params['id']);
      this.topic!.postDate = moment(this.topic?.postDate).fromNow();
      this.topic!.answers.sort((a, b) => (a.postDate > b.postDate) ? 1 : -1)
      this.topic?.answers.forEach(answer => answer.postDate = moment(answer.postDate).fromNow());
      if (!this.authenticationService.isLogged) this.topic!.answers = this.topic!.answers.filter(answer => !answer.reported);
    })
  }

  async addAnswer() {
    this.activeRoute.params.subscribe(async params => {
      const topicResponse = await this.topicService.addAnswer(params['id'], this.answerContent, moment().format());
      topicResponse.postDate = moment(topicResponse.postDate).fromNow();
      this.topic?.answers.push(topicResponse);
    })
    this.answerContent = "";
  }

  async reportTopic() {
    if (!this.authenticationService.isLogged) {
      this.router.navigate(['login']);
    } else {
      this.activeRoute.params.subscribe(async params => {
        await this.topicService.reportTopic(params['id']);
      })
      this.topic!.isReported = true;
    }
  }

  async reportAnswer(answerId: string) {
    if (!this.authenticationService.isLogged) {
      this.router.navigate(['login']);
    } else {
      this.activeRoute.params.subscribe(async params => {
        await this.topicService.reportAnswer(params['id'], answerId);
      })
      this.topic!.answers.find(answer => answer.tid === answerId)!.reported = true;
    }
  }

  async deleteTopic(topicId: string) {
    if (confirm("Are you sure you want to delete this topic?")) {
      await this.topicService.deleteTopic(topicId);
      this.router.navigate(['']);
    }
  }

  async deleteAnswer(topicId: string, answerTid: string) {
    if (confirm("Are you sure you want to delete this answer?")) {
      await this.topicService.deleteAnswer(topicId, answerTid);
      this.getTopic();
    }
  }

  ngOnInit(): void {
    this.getTopic();
  }

}
