import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import * as moment from 'moment';
import { Topic } from 'src/app/models/topic';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { TopicService } from 'src/app/services/topic.service';

@Component({
  selector: 'app-add-topic',
  templateUrl: './add-topic.component.html',
  styleUrls: ['./add-topic.component.scss']
})
export class AddTopicComponent implements OnInit {
  topic: Topic = {
    tid: "",
    title: "",
    postDate: "",
    author: "",
    content: "",
    language: "",
    isReported: false,
    answers: []
  }

  constructor(public topicService: TopicService, private router: Router, private authenticationService: AuthenticationService) {
   }

  async addTopic() {
    this.topic.author = JSON.parse(sessionStorage.getItem('currentUser') || '').username
    await this.topicService.addTopic(this.topic);
    this.router.navigate(['/']);
  }

  ngOnInit(): void {
    if (!this.authenticationService.isLogged) this.router.navigate(['/login'])
  }

}
