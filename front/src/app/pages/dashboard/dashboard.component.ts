import { Component, OnInit } from '@angular/core';
import { TopicService } from 'src/app/services/topic.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  selectedLanguage = ""

  constructor(public topicService: TopicService) { }

  async updateList() {
    console.log(this.selectedLanguage);
    this.topicService.topics = await this.topicService.fetchTopicsByTag(this.selectedLanguage)
  }

  ngOnInit(): void {
  }

}
