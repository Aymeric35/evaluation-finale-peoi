import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  user: User = {
    username: "",
    email: "",
    password: ""
  };

  constructor(private authenticationService: AuthenticationService, private router: Router) { }

  async registerUser() {
    await this.authenticationService.register(this.user);
    this.router.navigate(['/login']);
  }

  ngOnInit(): void {
  }

}
