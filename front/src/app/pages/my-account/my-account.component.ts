import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.scss']
})
export class MyAccountComponent implements OnInit {
  author: string = JSON.parse(sessionStorage.getItem('currentUser') || '').username

  constructor() { }

  ngOnInit(): void {
  }

}
