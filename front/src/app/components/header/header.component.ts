import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { TopicService } from 'src/app/services/topic.service';
import { TopicsListComponent } from '../topics-list/topics-list.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  search: string = ''

  constructor(public authenticationService: AuthenticationService, private topicService: TopicService, public router: Router) { }

  async onKeyupSearch(): Promise<void> {
    if (this.search === '') {
      this.topicService.topics = await this.topicService.getTopics();
    } else {
      this.topicService.topics = await this.topicService.fetchTopicsByTitle(this.search);
    }
  }

  async submitSearch(): Promise<void> {
    await this.router.navigate(['search-results']);
    this.topicService.topics = await this.topicService.fetchTopicsByTitle(this.search);
  }

  ngOnInit(): void {
  }

}
