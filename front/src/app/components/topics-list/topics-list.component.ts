import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { Topic } from 'src/app/models/topic';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { TopicService } from 'src/app/services/topic.service';


@Component({
  selector: 'app-topics-list',
  templateUrl: './topics-list.component.html',
  styleUrls: ['./topics-list.component.scss']
})
export class TopicsListComponent implements OnInit {
  constructor(public topicService: TopicService, private router: Router, private authenticationService: AuthenticationService) { }

  async fetchTopics() {
    this.topicService.topics = await this.topicService.getTopics();
    if (!this.authenticationService.isLogged) this.topicService.topics = this.topicService.topics.filter(topic => !topic.isReported);
  }

  ngOnInit(): void {
    if (this.router.url === '/') this.fetchTopics();
  }

}
