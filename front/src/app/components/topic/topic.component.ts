import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { Topic } from 'src/app/models/topic';
import { TopicService } from 'src/app/services/topic.service';

@Component({
  selector: 'app-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.scss']
})
export class TopicComponent implements OnInit {
  @Input() topic: Topic = {
    tid: "",
    title: "",
    postDate: "",
    author: "",
    content: "",
    language: "",
    isReported: false,
    answers: []
  };

  constructor(public topicService: TopicService) { }


  ngOnInit(): void {
    this.topic.postDate = moment(this.topic.postDate).fromNow();
  }

}
