import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { AuthenticationService } from "../services/authentication.service";

export const authenticationInterceptor =
    (authenticationService: AuthenticationService) => {
        axios.interceptors.request.use(
            (config: AxiosRequestConfig) => {
                if (config.headers && config.method !== 'get')
                    config.headers['Authorization'] = authenticationService.getCurrentUserBasicAuthentication();
                return config
            },
            (error: AxiosError) => {
                console.error('ERROR:', error)
                Promise.reject(error)
            })
    }