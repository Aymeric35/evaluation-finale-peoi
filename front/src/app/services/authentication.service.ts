import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import axios from 'axios';
import { User } from '../models/user';
import { UserAuth } from '../models/userAuth';
import { NotifierService } from 'angular-notifier';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private readonly SESSION_STORAGE_KEY = 'currentUser';
  public isLogged: boolean = false;
  public currentLoggedUser!: UserAuth;
  private readonly notifier: NotifierService;

  constructor(private router: Router, notifierService: NotifierService) {
    this.notifier = notifierService;
   }

  async register(user: User) {
    try {
      const response = await axios.post("http://localhost:8080/api/users", {
        username: user.username,
        email: user.email,
        password: user.password
      });
      return response.data;
    } catch (error) {
      return error;
    }
  }

  async credentialsValidation(user: UserAuth) {
    return axios.get("http://localhost:8080/api/users/me", {
      auth: {
        username: user.username,
        password: user.password
      }
    })
  }

  async login(user: UserAuth) {
    await this.credentialsValidation(user).then(response => {
      sessionStorage.setItem(this.SESSION_STORAGE_KEY, JSON.stringify(user));
      sessionStorage.setItem('isLogged', 'true');
      this.isLogged = true;
      this.currentLoggedUser = user;
      this.router.navigate([''])
      this.notifier.notify('success', 'Successfully logged in!');
    }).catch(error => {
      this.notifier.notify('error', "Incorrect credentials");
    });
  }

  logout() {
    sessionStorage.removeItem(this.SESSION_STORAGE_KEY);
    sessionStorage.setItem('isLogged', 'false');
    this.isLogged = false;
    this.router.url === '/' ?
    this.router.navigate(['']).then(() => window.location.reload()) : this.router.navigate(['']);
  }

  getCurrentUserBasicAuthentication(): string {
    const currentUserPlain = sessionStorage.getItem(this.SESSION_STORAGE_KEY);
    if (currentUserPlain) {
      const currentUser = JSON.parse(currentUserPlain);
      return "Basic " + btoa(currentUser.username + ":" + currentUser.password)
    } else {
      return "";
    }
  }
}
