import { Injectable } from '@angular/core';
import { NotifierService } from 'angular-notifier';
import axios from 'axios';
import { Answer } from '../models/answer';
import { Topic } from '../models/topic';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class TopicService {
  topics: Topic[] = [];
  private readonly notifier: NotifierService;

  constructor(notifierService: NotifierService) {
    this.notifier = notifierService;
   }

  public async getTopics() {
    const response = await axios.get("http://localhost:8080/api/topics");
    try {
      return response.data;
    } catch (error) {
      console.log(error);
    }
  }

  public async getTopicById(id: number) {
    const response = await axios.get("http://localhost:8080/api/topics/" + id);
    try {
      return response.data;
    } catch (error) {
      console.log(error);
    }
  }

  public addTopic(topic: Topic) {
    return axios.post("http://localhost:8080/api/topics/", {
      title: topic.title,
      content: topic.content,
      language: topic.language
    })
      .then( (response) => {
        this.notifier.notify('success', 'Your topic was successfully created');
        return response.data
      })
      .catch( (error) => {
        this.notifier.notify('error', error.response.data.message);
        return error;
      });
  }

  public addAnswer(id: number, content: string, date: string) {
    return axios.post("http://localhost:8080/api/topics/" + id + "/answer", {
      content: content,
      postDate: date
    })
      .then(function (response) {
        return response.data
      })
      .catch( (error) => {
        this.notifier.notify('error', error.response.data.message);
        return error;
      });
  }

  public async fetchTopicsByTitle(title: string) {
    return axios.get(`http://localhost:8080/api/topics`, {
      params: {
        title: title
      }
    })
      .then((res) => {
        console.log(res.data);
        return res.data;
      })
      .catch((err => {
        console.log(err);
        return err;
      }))
  }

  public async fetchTopicsByTag(language: string) {
    return axios.get(`http://localhost:8080/api/topics`, {
      params: {
        language: language
      }
    })
      .then((res) => {
        console.log(res.data);
        return res.data;
      })
      .catch((err => {
        console.log(err);
        return err;
      }))
  }

  public reportTopic(topicId: number) {
    return axios.post("http://localhost:8080/api/topics/" + topicId + "/report/")
      .then( (response) => {
        this.notifier.notify('success', 'Successfully reported!');
        return response.data
      })
      .catch( (error) => {
        this.notifier.notify('error', error.response.data.message);
        return error;
      });
  }

  public reportAnswer(topicId: number, answerId: string) {
    return axios.post("http://localhost:8080/api/topics/" + topicId + "/answer/" + answerId + "/report/")
      .then( (response) => {
        this.notifier.notify('success', 'Successfully reported!');
        return response.data
      })
      .catch( (error) => {
        this.notifier.notify('error', error.response.data.message);
        return error;
      });
  }

  public deleteTopic(topicId: string) {
    return axios.delete(`http://localhost:8080/api/topics/${topicId}`)
      .then( (response) => {
        this.notifier.notify('success', 'Successfully deleted!');
        return response
      })
      .catch( (error) => {
        this.notifier.notify('error', 'Something went wrong.');
        return error;
      });
  }

  public deleteAnswer(topicId: string, answerId: string) {
    return axios.delete(`http://localhost:8080/api/topics/${topicId}/answer/${answerId}`)
      .then( (response) => {
        this.notifier.notify('success', 'Successfully deleted!');
        return response
      })
      .catch( (error) => {
        this.notifier.notify('error', 'Something went wrong.');
        return error;
      });
  }
}
