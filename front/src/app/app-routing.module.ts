import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddTopicComponent } from './pages/add-topic/add-topic.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LoginComponent } from './pages/login/login.component';
import { MyAccountComponent } from './pages/my-account/my-account.component';
import { RegisterComponent } from './pages/register/register.component';
import { SearchComponent } from './pages/search/search.component';
import { TopicDetailsComponent } from './pages/topic-details/topic-details.component';

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'new-topic', component: AddTopicComponent},
  { path: 'topics/:id', component: TopicDetailsComponent},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'my-account', component: MyAccountComponent},
  { path: 'search-results', component: SearchComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
