export interface Answer {
    "tid": string,
	"content": string,
	"postDate": string,
	"topicTid": string,
	"reported": boolean,
	"user": {
		"username": string,
		"email": string
	}
}