import { Answer } from "./answer";

export interface Topic {
    tid: string,
    title: string,
    author: string,
    content: string,
    language: string,
    postDate: string,
    isReported: boolean,
    answers: Answer[]
}