import { Component } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { authenticationInterceptor } from './helpers/authentication.interceptor';
import { errorInterceptor } from './helpers/error.interceptor';
import { AuthenticationService } from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Zeniflow';

  constructor(private authenticationService: AuthenticationService, private router: Router) {
    authenticationInterceptor(this.authenticationService);
    errorInterceptor(this.authenticationService, this.router);
  }

  ngOnInit(): void {
    moment.locale('en-gb');
    if (sessionStorage.getItem('isLogged') === 'true') {
      this.authenticationService.isLogged = true;
    }
  }
}
