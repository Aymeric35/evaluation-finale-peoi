package com.zenika.academy.barbajavas.zeniflow.web.topics;

import com.zenika.academy.barbajavas.zeniflow.domain.models.answer.Answer;
import com.zenika.academy.barbajavas.zeniflow.domain.models.topic.TopicLanguage;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

public record GetTopicDto(String tid, String title, String content, TopicLanguage language, Timestamp postDate, String author, String email, Boolean isReported, Set<Answer> answers) {
}
