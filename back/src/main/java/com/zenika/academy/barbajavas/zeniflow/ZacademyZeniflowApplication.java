package com.zenika.academy.barbajavas.zeniflow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZacademyZeniflowApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZacademyZeniflowApplication.class, args);
	}

}
