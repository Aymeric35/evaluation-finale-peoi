package com.zenika.academy.barbajavas.zeniflow.web.topics;

import com.zenika.academy.barbajavas.zeniflow.domain.models.topic.TopicLanguage;

import java.sql.Timestamp;

public record NewTopicDto(String title, String content, TopicLanguage language) {
}
