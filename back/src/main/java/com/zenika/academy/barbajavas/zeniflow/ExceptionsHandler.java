package com.zenika.academy.barbajavas.zeniflow;

import com.zenika.academy.barbajavas.zeniflow.domain.models.answer.AnswerAlreadyReportedException;
import com.zenika.academy.barbajavas.zeniflow.domain.models.answer.BadLengthAnswerContentException;
import com.zenika.academy.barbajavas.zeniflow.domain.models.answer.NotDeletableAnswer;
import com.zenika.academy.barbajavas.zeniflow.domain.models.answer.NullAnswerException;
import com.zenika.academy.barbajavas.zeniflow.domain.models.topic.BadLengthTopicTitleException;
import com.zenika.academy.barbajavas.zeniflow.domain.models.topic.NotDeletableTopic;
import com.zenika.academy.barbajavas.zeniflow.domain.models.topic.NullTopicException;
import com.zenika.academy.barbajavas.zeniflow.domain.models.topic.TopicAlreadyReportedException;
import com.zenika.academy.barbajavas.zeniflow.domain.models.user.NullUserRequestException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionsHandler {
    @ExceptionHandler(value = BadLengthTopicTitleException.class)
    public final ResponseEntity<ApiError> badLengthTopicTitleException(BadLengthTopicTitleException e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "Your question must not exceed 20 words."), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = BadLengthAnswerContentException.class)
    public final ResponseEntity<ApiError> badLengthAnswerContentException(BadLengthAnswerContentException e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "Your answer must not exceed 100 words."), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = NullUserRequestException.class)
    public final ResponseEntity<ApiError> nullUserRequestException(NullUserRequestException e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.UNAUTHORIZED, "You must be authenticated to do the following action."), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = NullTopicException.class)
    public final ResponseEntity<ApiError> nullTopicException(NullTopicException e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "The topic does not exist."), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = TopicAlreadyReportedException.class)
    public final ResponseEntity<ApiError> topicAlreadyReportedException(TopicAlreadyReportedException e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "The topic has already been reported."), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = NotDeletableTopic.class)
    public final ResponseEntity<ApiError> notDeletableTopic(NotDeletableTopic e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "You must report the topic before deleting it."), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = NullAnswerException.class)
    public final ResponseEntity<ApiError> nullAnswerException(NullAnswerException e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "The answer does not exist."), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = AnswerAlreadyReportedException.class)
    public final ResponseEntity<ApiError> answerAlreadyReportedException(AnswerAlreadyReportedException e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "The answer has already been reported."), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = NotDeletableAnswer.class)
    public final ResponseEntity<ApiError> notDeletableAnswer(NotDeletableAnswer e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "You must report the answer before deleting it."), HttpStatus.BAD_REQUEST);
    }
}
