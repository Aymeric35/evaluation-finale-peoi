package com.zenika.academy.barbajavas.zeniflow.web.topics;

import com.zenika.academy.barbajavas.zeniflow.application.TopicService;
import com.zenika.academy.barbajavas.zeniflow.domain.models.topic.*;
import com.zenika.academy.barbajavas.zeniflow.domain.models.user.NullUserRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping
public class TopicController {
    private final TopicService topicService;

    @Autowired
    public TopicController(TopicService topicService) {
        this.topicService = topicService;
    }

    @PostMapping("/topics")
    ResponseEntity<GetTopicDto> createNewTopic(@RequestBody NewTopicDto newTopicDto) throws BadLengthTopicTitleException, NullUserRequestException {
        Topic topic = topicService.createNewTopic(
                newTopicDto.title(), newTopicDto.content(), newTopicDto.language()
        );
        return ResponseEntity.ok(new GetTopicDto(
                topic.getTid(), topic.getTitle(), topic.getContent(), topic.getLanguage(), topic.getPostDate(), topic.getUser().getUsername(), topic.getUser().getEmail(), topic.isReported(), topic.getAnswers()
        ));
    }

    @GetMapping("/topics")
    ResponseEntity<Stream<GetTopicDto>> getAllTopics(@RequestParam(name = "title", required = false) String title, @RequestParam(name = "language", required = false) TopicLanguage language) {
        if (title != null) {
            return ResponseEntity.ok(
                    StreamSupport.stream(topicService.getAllTopicsByTitle(title).spliterator(), false).map(
                            topic -> new GetTopicDto(
                                    topic.getTid(), topic.getTitle(), topic.getContent(), topic.getLanguage(), topic.getPostDate(), topic.getUser().getUsername(), topic.getUser().getEmail(), topic.isReported(), topic.getAnswers()
                            )
                    )
            );
        } else if (language != null) {
            return ResponseEntity.ok(
                    StreamSupport.stream(topicService.getAllTopicsByTag(language).spliterator(), false).map(
                            topic -> new GetTopicDto(
                                    topic.getTid(), topic.getTitle(), topic.getContent(), topic.getLanguage(), topic.getPostDate(), topic.getUser().getUsername(), topic.getUser().getEmail(), topic.isReported(), topic.getAnswers()
                            )
                    )
            );
        } else {
            return ResponseEntity.ok(
                    StreamSupport.stream(topicService.getAllTopics().spliterator(), false).map(
                            topic -> new GetTopicDto(
                                    topic.getTid(), topic.getTitle(), topic.getContent(), topic.getLanguage(), topic.getPostDate(), topic.getUser().getUsername(), topic.getUser().getEmail(), topic.isReported(), topic.getAnswers()
                            )
                    )
            );
        }
    }

    @GetMapping("/topics/{tid}")
    ResponseEntity<GetTopicDto> getTopicById(@PathVariable String tid) throws NullTopicException {
        Topic topic = topicService.getTopicById(tid);
        return ResponseEntity.ok(
                new GetTopicDto(topic.getTid(), topic.getTitle(), topic.getContent(), topic.getLanguage(), topic.getPostDate(), topic.getUser().getUsername(), topic.getUser().getEmail(), topic.isReported(), topic.getAnswers())
        );
    }

    @PostMapping("/topics/{tid}/report")
    void reportTopic(@PathVariable String tid) throws NullTopicException, TopicAlreadyReportedException {
        topicService.reportTopic(tid);
    }

    @DeleteMapping("topics/{tid}")
    void deleteTopic(@PathVariable String tid) throws NullTopicException, NotDeletableTopic {
        topicService.deleteTopic(tid);
    }
 }
