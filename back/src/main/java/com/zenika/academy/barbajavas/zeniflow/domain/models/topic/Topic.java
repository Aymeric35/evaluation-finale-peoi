package com.zenika.academy.barbajavas.zeniflow.domain.models.topic;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.zenika.academy.barbajavas.zeniflow.domain.models.answer.Answer;
import com.zenika.academy.barbajavas.zeniflow.domain.models.user.User;
import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Table(name = "topics")
@NoArgsConstructor
@Getter
public class Topic {
    @Id
    private String tid;
    private String title;
    private String content;
    private Timestamp postDate;
    @Enumerated(EnumType.STRING)
    private TopicLanguage language;
    @ManyToOne
    @JoinColumn(name = "user_tid")
    private User user;
    @OneToMany(mappedBy="topic")
    @JsonManagedReference
    private Set<Answer> answers;
    @Setter
    private boolean isReported;

    public Topic(String tid, String title, String content, TopicLanguage language, Timestamp postDate, User user) {
        this.tid = tid;
        this.title = title;
        this.content = content;
        this.language = language;
        this.postDate = postDate;
        this.user = user;
        this.isReported = false;
    }

    public void addAnswerToTopic(Answer answer) {
        answers.add(answer);
    }
}
