package com.zenika.academy.barbajavas.zeniflow.web.answers;

public record NewAnswerDto(String content) {
}
