package com.zenika.academy.barbajavas.zeniflow.web.users;

import com.zenika.academy.barbajavas.zeniflow.application.UserService;
import com.zenika.academy.barbajavas.zeniflow.domain.models.user.NullUserRequestException;
import com.zenika.academy.barbajavas.zeniflow.domain.models.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping
public class UserController {
    private final UserService userService;
    private final PasswordEncoder encoder;

    @Autowired
    public UserController(UserService userService, PasswordEncoder encoder) {
        this.userService = userService;
        this.encoder = encoder;
    }

    @PostMapping("/users/me")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<GetUserDto> me() throws NullUserRequestException {
        User user = userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(NullUserRequestException::new);
        return ResponseEntity.ok(new GetUserDto(user.getUsername(), user.getEmail(), user.getTid()));
    }

    @PostMapping("/users")
    ResponseEntity<User> createNewUser(@RequestBody NewUserDto newUserDto) {
        return ResponseEntity.ok(userService.createNewUser(newUserDto.tid(), newUserDto.username(), newUserDto.email(), encoder.encode(newUserDto.password())));
    }

    @GetMapping("/users")
    ResponseEntity<Iterable<User>> getUsers() {
        return ResponseEntity.ok(userService.getUsers());
    }

    @GetMapping("/users/{tid}")
    ResponseEntity<Optional<User>> getUserById(@PathVariable String tid) {
        return ResponseEntity.ok(userService.findById(tid));
    }

    @DeleteMapping("/users/{tid}")
    void deleteUserById(@PathVariable String tid) {
        userService.deleteUser(tid);
    }
}
