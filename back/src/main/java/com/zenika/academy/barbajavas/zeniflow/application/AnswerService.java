package com.zenika.academy.barbajavas.zeniflow.application;

import com.zenika.academy.barbajavas.zeniflow.domain.models.answer.*;
import com.zenika.academy.barbajavas.zeniflow.domain.models.topic.NullTopicException;
import com.zenika.academy.barbajavas.zeniflow.domain.models.topic.Topic;
import com.zenika.academy.barbajavas.zeniflow.domain.models.user.NullUserRequestException;
import com.zenika.academy.barbajavas.zeniflow.domain.models.user.User;
import com.zenika.academy.barbajavas.zeniflow.domain.repositories.AnswerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;

@Component
public class AnswerService {
    private final AnswerRepository answerRepository;
    private final UserService userService;
    private final TopicService topicService;

    @Autowired
    public AnswerService(AnswerRepository answerRepository, UserService userService, TopicService topicService) {
        this.answerRepository = answerRepository;
        this.userService = userService;
        this.topicService = topicService;
    }

    public Answer createNewAnswer(String content, String topicTid) throws NullUserRequestException, NullTopicException, BadLengthAnswerContentException {
        if (checkAnswerNbOfWords(content) > 100) throw new BadLengthAnswerContentException();
        User user = userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(NullUserRequestException::new);
        Topic topic = topicService.getTopicById(topicTid);
        Answer answer = new Answer(UUID.randomUUID().toString(), content, Timestamp.from(Instant.now()), user, topic);
        topic.addAnswerToTopic(answer);
        answerRepository.save(answer);
        return answer;
    }

    public Iterable<Topic> getAllAnswersByTopicId() {
        return null;
    }

    public void reportAnswer(String tid) throws NullAnswerException, AnswerAlreadyReportedException {
        Answer answer = answerRepository.findById(tid).orElseThrow(NullAnswerException::new);
        if (answer.isReported()) throw new AnswerAlreadyReportedException();
        answer.setReported(true);
        answerRepository.save(answer);
    }

    public void deleteAnswer(String tid) throws NullAnswerException, NotDeletableAnswer {
        Answer answer = answerRepository.findById(tid).orElseThrow(NullAnswerException::new);
        if (answer.isReported()) {
            answerRepository.deleteById(tid);
        } else {
            throw new NotDeletableAnswer();
        }

    }

    private int checkAnswerNbOfWords(String s) {
        String trim = s.trim();
        return trim.isEmpty() ? 0 : trim.split("\\s+").length;
    }
}
