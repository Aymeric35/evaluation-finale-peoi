package com.zenika.academy.barbajavas.zeniflow.web.answers;

import com.zenika.academy.barbajavas.zeniflow.domain.models.user.User;
import com.zenika.academy.barbajavas.zeniflow.web.users.GetUserDto;

import java.sql.Timestamp;

public record GetAnswerDto(String tid, String content, Timestamp postDate, GetUserDto user, String topicTid) {
}
