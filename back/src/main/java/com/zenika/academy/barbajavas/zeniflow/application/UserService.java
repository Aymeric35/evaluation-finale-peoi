package com.zenika.academy.barbajavas.zeniflow.application;

import com.zenika.academy.barbajavas.zeniflow.domain.models.user.User;
import com.zenika.academy.barbajavas.zeniflow.domain.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
public class UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User createNewUser(String tid, String username, String email, String password) {
        User user = new User(UUID.randomUUID().toString(), username, email, password);
        userRepository.save(user);
        return user;
    }

    public Optional<User> getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public Iterable<User> getUsers() {
        return userRepository.findAll();
    }

    public Optional<User> findById(String tid) {
        return userRepository.findById(tid);
    }

    public void deleteUser(String tid) {
        userRepository.deleteById(tid);
    }

}
