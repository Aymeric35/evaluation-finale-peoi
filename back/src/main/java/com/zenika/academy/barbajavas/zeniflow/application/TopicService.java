package com.zenika.academy.barbajavas.zeniflow.application;

import com.zenika.academy.barbajavas.zeniflow.domain.models.topic.*;
import com.zenika.academy.barbajavas.zeniflow.domain.models.user.NullUserRequestException;
import com.zenika.academy.barbajavas.zeniflow.domain.models.user.User;
import com.zenika.academy.barbajavas.zeniflow.domain.repositories.AnswerRepository;
import com.zenika.academy.barbajavas.zeniflow.domain.repositories.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;

@Component
public class TopicService {
    private final TopicRepository topicRepository;
    private final UserService userService;
    private final AnswerRepository answerRepository;

    @Autowired
    public TopicService(TopicRepository topicRepository, UserService userService, AnswerRepository answerRepository) {
        this.topicRepository = topicRepository;
        this.userService = userService;
        this.answerRepository = answerRepository;
    }

    public Topic createNewTopic(String title, String content, TopicLanguage language) throws NullUserRequestException, BadLengthTopicTitleException {
        if (checkTitleNbOfWords(title) > 20) throw new BadLengthTopicTitleException();
        if (!title.endsWith("?")) title += " ?";
        User user = userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(NullUserRequestException::new);
        Topic topic = new Topic(UUID.randomUUID().toString(), title, content, language, Timestamp.from(Instant.now()), user);
        topicRepository.save(topic);
        return topic;
    }

    public Iterable<Topic> getAllTopics() {
        return topicRepository.findAll(Sort.by(Sort.Direction.DESC, "postDate"));
    }

    public Topic getTopicById(String tid) throws NullTopicException {
        return topicRepository.findById(tid).orElseThrow(NullTopicException::new);
    }

    public Iterable<Topic> getAllTopicsByTitle(String title){
        return topicRepository.findByTitle(title);
    }

    public Iterable<Topic> getAllTopicsByTag(TopicLanguage language){
        return topicRepository.findByLanguage(language);
    }

    public void reportTopic(String tid) throws NullTopicException, TopicAlreadyReportedException {
        Topic topic = topicRepository.findById(tid).orElseThrow(NullTopicException::new);
        if (topic.isReported()) throw new TopicAlreadyReportedException();
        topic.setReported(true);
        topicRepository.save(topic);
    }

    public void deleteTopic(String tid) throws NullTopicException, NotDeletableTopic {
        Topic topic = topicRepository.findById(tid).orElseThrow(NullTopicException::new);
        if (topic.isReported()) {
            answerRepository.deleteByTopicTid(tid);
            topicRepository.deleteById(tid);
        } else {
            throw new NotDeletableTopic();
        }
    }

    private int checkTitleNbOfWords(String s) {
        String trim = s.trim();
        return trim.isEmpty() ? 0 : trim.split("\\s+").length;
    }
}
