package com.zenika.academy.barbajavas.zeniflow.web.answers;

import com.zenika.academy.barbajavas.zeniflow.application.AnswerService;
import com.zenika.academy.barbajavas.zeniflow.domain.models.answer.*;
import com.zenika.academy.barbajavas.zeniflow.domain.models.topic.NullTopicException;
import com.zenika.academy.barbajavas.zeniflow.domain.models.topic.Topic;
import com.zenika.academy.barbajavas.zeniflow.domain.models.user.NullUserRequestException;
import com.zenika.academy.barbajavas.zeniflow.web.topics.GetTopicDto;
import com.zenika.academy.barbajavas.zeniflow.web.users.GetUserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping
public class AnswerController {
    private final AnswerService answerService;

    @Autowired
    public AnswerController(AnswerService answerService) {
        this.answerService = answerService;
    }


    @PostMapping("/topics/{tid}/answer")
    ResponseEntity<GetAnswerDto> postNewAnswer(@PathVariable String tid, @RequestBody NewAnswerDto newAnswerDto) throws NullTopicException, NullUserRequestException, BadLengthAnswerContentException {
        Answer answer = answerService.createNewAnswer(
                newAnswerDto.content(), tid
        );
        return ResponseEntity.ok(new GetAnswerDto(
                answer.getTid(), answer.getContent(), answer.getPostDate(), new GetUserDto(answer.getUser().getUsername(), answer.getUser().getEmail(), answer.getUser().getTid()), answer.getTopic().getTid()
        ));
    }

    @PostMapping("/topics/{topicTid}/answer/{answerTid}/report")
    void reportAnswer(@PathVariable String answerTid) throws NullAnswerException, AnswerAlreadyReportedException {
        answerService.reportAnswer(answerTid);
    }

    @DeleteMapping("topics/{topicTid}/answer/{answerTid}")
    void deleteAnswer(@PathVariable String answerTid) throws NullAnswerException, NotDeletableAnswer {
        answerService.deleteAnswer(answerTid);
    }
}
