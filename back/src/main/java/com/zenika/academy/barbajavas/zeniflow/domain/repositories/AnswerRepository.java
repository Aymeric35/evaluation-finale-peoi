package com.zenika.academy.barbajavas.zeniflow.domain.repositories;

import com.zenika.academy.barbajavas.zeniflow.domain.models.answer.Answer;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface AnswerRepository extends CrudRepository<Answer, String> {
    @Transactional
    void deleteByTopicTid(String tid);
}
