package com.zenika.academy.barbajavas.zeniflow.web.users;

import com.zenika.academy.barbajavas.zeniflow.domain.models.user.User;

public record GetUserDto(String username, String email, String tid) {
}
