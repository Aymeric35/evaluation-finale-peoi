package com.zenika.academy.barbajavas.zeniflow.domain.repositories;

import com.zenika.academy.barbajavas.zeniflow.domain.models.topic.Topic;
import com.zenika.academy.barbajavas.zeniflow.domain.models.topic.TopicLanguage;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TopicRepository extends CrudRepository<Topic, String> {
    Iterable<Topic> findAll(Sort by);
    @Query(value = "SELECT *\n" +
            "FROM topics\n" +
            "WHERE word_similarity(?1, topics.title) > 0.4",
            nativeQuery = true)
    Iterable<Topic> findByTitle(String title);
    Iterable<Topic> findByLanguage(TopicLanguage language);
}
