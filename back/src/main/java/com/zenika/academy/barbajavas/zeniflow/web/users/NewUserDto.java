package com.zenika.academy.barbajavas.zeniflow.web.users;

public record NewUserDto(String tid, String username, String email, String password) {
}
