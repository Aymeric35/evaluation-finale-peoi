package com.zenika.academy.barbajavas.zeniflow.domain.models.topic;

public enum TopicLanguage {
    JAVA, SPRING, JAVASCRIPT, HTML, CSS, NODEJS, PHP, RUBY, NONE
}
