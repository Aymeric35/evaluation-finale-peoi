package com.zenika.academy.barbajavas.zeniflow.domain.models.answer;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zenika.academy.barbajavas.zeniflow.domain.models.topic.Topic;
import com.zenika.academy.barbajavas.zeniflow.domain.models.user.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "answers")
@NoArgsConstructor
@Getter
public class Answer {
    @Id
    private String tid;
    private String content;
    private Timestamp postDate;
    @ManyToOne
    @JoinColumn(name = "user_tid")
    private User user;
    @ManyToOne
    @JoinColumn(name = "topic_tid")
    @JsonBackReference
    private Topic topic;
    @Setter
    private boolean isReported;

    public Answer(String tid, String content, Timestamp postDate, User user, Topic topic) {
        this.tid = tid;
        this.content = content;
        this.postDate = postDate;
        this.user = user;
        this.topic = topic;
        this.isReported = false;
    }
}
