package com.zenika.academy.barbajavas.zeniflow.domain.repositories;

import com.zenika.academy.barbajavas.zeniflow.domain.models.user.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, String> {
    Optional<User> findByUsername(String username);
}
