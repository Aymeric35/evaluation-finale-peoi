--drop table if exists answers;
--drop table if exists topics;
--drop table if exists users;

CREATE EXTENSION IF NOT EXISTS pg_trgm;

create table if not exists users(
    tid char(36) primary key,
    username text not null,
    email text not null unique,
    password text not null
);

create table if not exists topics(
    tid char(36) primary key,
    title text not null,
    content text not null,
    language text not null,
    post_date timestamp not null,
    is_reported boolean not null default false,
    user_tid char(36) not null references users(tid)
);

create table if not exists answers(
    tid char(36) primary key,
    content text not null,
    post_date timestamp not null,
    is_reported boolean not null default false,
    topic_tid char(36) not null references topics(tid),
    user_tid char(36) not null references users(tid)
);

